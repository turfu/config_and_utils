Config and utils
================

My personal config files and various Linux scripts.

Usage
-----
Install the repository at /root/scripts/config_and_utils, and run
/root/scripts/config_and_utils/utils/bin/deploy_utils.sh and
/root/scripts/config_and_utils/utils/bin/deploy_config.sh.

The config files do not include any personal information (e.g. no email
address).
Note that some files assume that there is a user named "turfu" and that
the hostname of the machine is "turfu".
