(setq make-backup-files nil)

(setq load-path (cons (expand-file-name "~/.emacs.d/elpa") load-path))
(require 'package)
(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)

(server-start)

; XXX, security
(setq select-enable-clipboard t)
(setq select-enable-primary t)

(column-number-mode t)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)

(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(global-whitespace-mode 1)
(setq whitespace-style
      (quote (
	      face
	      empty
	      indentation
	      lines-tail
	      space-before-tab
	      tabs
	      trailing
	      )))

(global-set-key (kbd "C-c RET") 'compile)
(global-set-key (kbd "C-x C-c") 'save-buffers-kill-emacs)
(global-set-key (kbd "C-c C-r") 'revert-buffer)

(setq-default
 c-basic-offset 8
 c-default-style "linux"
 indent-tabs-mode t
 tab-width 8
 )

(defun find-file-maybe-as-root ()
  "edit the file with root-privileges if the file is not writable by user"
  (interactive)
  (let ((file (ido-read-file-name "Edit as root: ")))
    (unless (file-writable-p file)
      (setq file (concat "/sudo::" file)))
    (find-file file)))
(global-set-key (kbd "C-x F") 'find-file-maybe-as-root)

(set-face-font
 'menu
 "-ADBO-Source Code Pro-normal-normal-normal-*-15-*-*-*-m-0-iso10646-1")
(set-face-font
 'default
 "-ADBO-Source Code Pro-normal-normal-normal-*-15-*-*-*-m-0-iso10646-1")

;#Automatically generated
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(confirm-kill-emacs (quote y-or-n-p))
 '(custom-enabled-themes (quote (wombat)))
 '(desktop-path (quote ("~/.emacs.d/desktop" "~")))
 '(desktop-save-mode t)
 '(font-use-system-font nil)
 ; XXX, useless ?
 '(indent-tabs-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cursor ((t (:background "#21C170")))))
