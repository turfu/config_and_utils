#!/bin/bash

test -n "$BASH" || exit 1

shopt -u expand_aliases

# Interactivity
shopt -s globstar
GLOBIGNORE=.:..

function remove_double_confirm {
	expect -f <(env cat <<EOF
set timeout -1
set args [lrange \$argv 0 end]
spawn -noecho {*}\$args

proc remove_double_confirm {} {
	interact {
		n { send "n\n"; return }
		a { send "a\n"; return }
		y { send "y\n"; return }
	}
}

expect {
	"? " {
		remove_double_confirm
		exp_continue
	}
	eof
}

catch wait err
exit [lindex \$err 3]
EOF
	) -- "$@"
}

function chown_to_me {
	declare tmpdir; tmpdir="$(mktemp -d)"
	(
		trap 'rmdir "$tmpdir"' EXIT
		GLOBIGNORE=.:..

		# XXX, race condition possible
		env mv "$@" "$tmpdir"
		env cp -a --no-preserve=ownership "$tmpdir"/* .
		env rm -rf "$tmpdir"/*
	)
}

alias cp='remove_double_confirm cp --reflink=auto --sparse=auto -i'
alias mv='remove_double_confirm mv -i'
alias rm='remove_double_confirm rm -i'
alias du_='btrfs filesystem du'

alias l_='ls -Fs --color=auto --group-directories-first'
alias ls='l_ -A'
alias la='ls -a'
# does not support files named '.*:object_r:.*'...
function ll {
	ls -lZ --color=always "$@" | sed s/:object_r:/::/
}

function cl {
	cd "$@" && ls
}

# Network
# user agent used by the Tor browser
declare -r USER_AGENT='Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0'

alias ping-test='watch -n 0.3 "ping -c 1 quad9.net | grep transmitted"'
alias youtube-dl="youtube-dl --user-agent \"$user_agent\""

# Development
alias git-commit="git commit --allow-empty-message -m ''"
alias git-grep='git grep --recurse-submodules'
alias git-log='git log --decorate --graph --all --show-signature'
alias git-log-oneline='git log --decorate --graph --all --oneline'
alias git-ls-files='git ls-files --recurse-submodules'
alias git-show='git show --show-signature'
function git-list-gpg-keys {
	git log --pretty='%G?:%aN <%aE> %GK'	\
		| grep -Ev "^N:"		\
		| cut -c3-			\
		| sort				\
		| uniq
}

# Docker
function _docker-run {
	declare docker_cmd="$1"; shift
	$docker_cmd run --rm -t -i "$@"
}
function _docker-run-x11 {
	declare docker_cmd="$1"; shift
	_docker-run "$docker_cmd"					\
		--mount=type=bind,src=/tmp/.X11-unix,dst=/tmp/.X11-unix	\
		-e DISPLAY="$DISPLAY" "$@"
}
function _docker-run-here {
	declare docker_cmd="$1"; shift
	_docker-run "$docker_cmd"					\
		--mount=type=bind,src="$PWD",dst=/home/work/ "$@"
}
function _docker-run-here-x11 {
	declare docker_cmd="$1"; shift
	_docker-run-x11 "$docker_cmd"					\
		--mount=type=bind,src="$PWD",dst=/home/work/ "$@"
}

alias docker-run='_docker-run docker'
alias docker-run-x11='_docker-run-x11 docker'
alias docker-run-here='_docker-run-here docker'
alias docker-run-here-x11='_docker-run-here-x11 docker'

# Packages
alias pacman='pacman --color=auto'
alias trizen='trizen --color=auto'

function pacman-sort-installed {
	\pacman -Qi | grep "Name\|Size" | cut -d: -f2 | paste - -	\
		| awk '{print $1, $2 $3}' | sort -hk 2
}

function arch-audit {
	(
		set -o pipefail
		/bin/arch-audit "$@" | {
			grep --color=auto -E '.*High.*|$' || true
		}
	)
}

# Utilities
alias cat='cat -v'
alias diff='diff --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias zgrep='zgrep --color=auto'

alias sed='sed --follow-symlinks'
alias cut_column='cut --complement'

# Terminal / X
alias fr='loadkeys fr; setxkbmap fr'
alias bepo='setxkbmap us intl && xmodmap ~/.Xmodmap'

alias term_clear="echo -ne '\033c'"

function set_win_title {
	echo -ne '\033]0;'
	echo -n "$@"
	echo -ne '\007'
	echo "$@"
}

# Desktop
alias xclip_select='xclip -selection clipboard'

alias scrot='scrot -q 100 -zcd'

alias hdmi_connect='xrandr --output HDMI-1 --mode 1920x1080'
alias hdmi_disconnect='xrandr --output HDMI-1 --off'

function redshift {
	declare -r color="$1"; shift

	echo -n "$color" > "$TMPDIR"/.redshift_temp

	env redshift -l 0:0 -rt "$color":"$color" "$@"
}

# Misc
# alias mpv='mpv --hwdec=vdpau --vo=gpu --loop-file=inf'
alias mpv='mpv --loop-file=inf'

function tokei_summary {
	local sep=
	local arg
	for arg in "$@"; do
		case "$arg" in
			\.|*\.git|*\.repo)	continue;;
			*)			;;
		esac
		arg="${arg#./}"
		echo -n "$sep$arg "
		sep=$'\n'
		tokei "$arg" | grep ^\ Total | awk '{ print $4 }'
	done | sort -hk2 | awk NF | {
		local output
		local total
		output="$(cat)"
		total="$(echo -n "$output" | awk '{ print $2 }')"
		echo "$output"
		echo -n 'Total: '
		echo -n "$output" | awk '{ print $2 }' | paste -sd+ | bc
	} | column -t
}

test -z "$B"		&& declare -ri B=1			|| true
test -z "$K"		&& declare -ri K=$((1024 * B))		|| true
test -z "$M"		&& declare -ri M=$((1024 * K))		|| true
test -z "$G"		&& declare -ri G=$((1024 * M))		|| true
test -z "$T"		&& declare -ri T=$((1024 * G))		|| true

test -z "$SEC"		&& declare -ri SEC=1			|| true
test -z "$MIN"		&& declare -ri MIN=$((60 * SEC))	|| true
test -z "$HOUR"		&& declare -ri HOUR=$((60 * MIN))	|| true
test -z "$DAY"		&& declare -ri DAY=$((24 * HOUR))	|| true
test -z "$WEEK"		&& declare -ri WEEK=$((7 * DAY))	|| true
test -z "$MONTH"	&& declare -ri MONTH=$((30 * DAY))	|| true
test -z "$YEAR"		&& declare -ri YEAR=$((365 * DAY + DAY / 4)) || true

function calc {
	local res=
	let "res = $@"
	echo "$res"
}

function add_nb_to_filename {
	mv "$2" "$1-$2"
}

function urandom_gen_pw {
	declare -r len="$1"
	</dev/urandom tr -dc '[:graph:]' | head -c "$len"
	echo
}
function urandom_gen_login {
	declare -r len="$1"
	</dev/urandom tr -dc '[:alnum:]_-' | head -c "$len"
	echo
}

# Customization
PATH+=":/usr/local/bin:$HOME/.local/bin"
export PATH
export LANG;	test -z "$LANG" && LANG=en_US.UTF-8 || true
export LC_ALL;	test -z "$LC_ALL" && LC_ALL="$LANG" || true

export EDITOR;	test -z "$EDITOR" && EDITOR=emacsclient || true
export VISUAL;	test -z "$VISUAL" && VISUAL=emacsclient || true

export TMPDIR;	test -z "$TMPDIR" && TMPDIR=/tmp/."$USER" || true

export LS_COLORS; test -z "$LS_COLORS" && LS_COLORS="$LS_COLORS:di=0;36:" || true

# Prompt
function COLOR {
	declare -r type_="$1"
	declare -r color_="$2"
	# man console_codes(4)
	echo -n "\[\e[$type_;$color_""m\]"
}

BOLD=1

BROWN="$(COLOR $BOLD 33)"
CYAN="$(COLOR $BOLD 36)"
RED="$(COLOR $BOLD 31)"

unset COLOR

declare PS1_PREFIX=
declare PS1_SUFFIX=

# PS1_PREFIX and PS1_SUFFIX may be used to customize the prompt
function set_ps1 {
	local err=$?

	local THIS_MACHINE; : "${THIS_MACHINE:=turfu}"

	local user=
	local user_color=
	local path_color="$CYAN"
	local sym=
	local sym_color=
	local cli_color="$CYAN"

	if [ "$HOSTNAME" == "$THIS_MACHINE" ]; then
		user=\\u
	else
		user=\\u@\\h
	fi

	if [ "$UID" -eq 0 ]; then
		user_color="$RED"
		sym_color="$RED"
		sym=\#
	else
		user_color="$BROWN"
		sym_color="$BROWN"
		sym=\$
	fi

	if [ ! "$err" -eq 0 ]; then
		user_color="$RED"
		path_color="$RED"
		sym_color="$RED"
	fi

	PS1="$PS1_PREFIX"
	PS1+="$user_color$user $path_color\W $sym_color$sym $cli_color"
	PS1+="$PS1_SUFFIX"
}

HISTORY_APPEND=(
	'IGNOREEOF=999'

	'cpupower frequency-info'
	'cpupower frequency-set -u 3.5ghz'

	'i3lock & systemctl suspend -i'
	'i3-msg exit'

	'cd /sys/class/backlight/intel_backlight'
	'tee brightness <<< 100'
	'redshift 2000'

	'ping -c1 9.9.9.9'
	'ping -c1 quad9.net'
	'ping-test'

	'gpgconf --kill gpg-agent'

	"pgrep '^ssh$'"
	"pkill '^ssh$'"
	'eval "$(ssh-agent)" && ssh-add ~/.ssh/id_ed25519'
	'scp /config/misc/vm_skel/* HOST:/root/'

	'pass show web/XXX | xclip_select'

	'git clone https://github.com/'
	'git remote show -n origin'
	'git diff --cached'
	'git fetch --all'
	'git add -u && git stash && git stash drop'
	'git commit -F /tmp/.commit'

	'docker ps -aq | head -n1 | xargs docker rm'

	'journalctl -xeu unit'
	'systemctl --user stop pulseaudio{,.socket}'
	'systemctl --user start pulseaudio'

	'sloccount . > .sloc'
	'tokei . > .tokei'
	'tokei_summary *'
	'ctags -eR .'

	'mpv --no-video --volume 60 XXX'

	'du -shc * | sort -h'
	'du_ -s * | sort -hk1'

	"( IFS=\$'\n' find -type f | xargs -d\$'\n' -L1 grep EXPR )"

	'set_win_title urxvt'
)

# XXX, race condition possible
function update_history {
	if [ -z "$HISTFILE" ]; then
		return
	fi

	history -w

	if [ ! -e "$HISTFILE" ]; then
		touch "$HISTFILE"
	fi
	mapfile -t < "$HISTFILE"

	declare -i i
	for ((i=0; i < ${#HISTORY_APPEND[@]}; ++i)); do
		# MAPFILE[0] will be discarded when the previous user
		# command will be appended to the history file, which
		# will happen at the end of this function.
		# XXX, verify it ^
		MAPFILE["$((i + 1))"]="${HISTORY_APPEND[i]}"
	done

	printf "%s\n" "${MAPFILE[@]}" > "$HISTFILE"

	history -r
}

function pre_prompt {
	set_ps1
	update_history
}

PROMPT_COMMAND=pre_prompt

trap 'echo -ne "\e[0m"' DEBUG

test -f /config/local/.bashrc && source /config/local/.bashrc || true
test -f ~/.local/.bashrc && source ~/.local/.bashrc || true

function do_cmd_in_cli {
	history -s "$@"
	echo -n "${PS1@P}"
	# XXX, spaces are not printed
	echo "$@"
	"$@"
}

set_ps1
shopt -s expand_aliases
set -o history
