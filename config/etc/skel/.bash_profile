#!/bin/bash

TMPDIR=/tmp/."$USER"
mkdir -p "$TMPDIR"
chmod o= "$TMPDIR"

if [ -z "$DISPLAY" ]; then
	exec startx
fi

function is_running_interactively {
	[[ -n "$PS1" && "$-" =~ i ]]
}

if [ -n "$BASH" ] && is_running_interactively && [ -f ~/.bashrc ]; then
	source ~/.bashrc
fi
