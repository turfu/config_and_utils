#!/bin/sh

set -e

cd /sys/class/backlight/intel_backlight

if [ ! -e brightness ]; then
	exit 1
fi

chown root:backlight brightness
chmod g+w brightness
