#!/bin/sh

set -e

mkdir -p -m u=rwx,g=rwx,o= /tmp/.pulse
chown turfu:pulse /tmp/.pulse
