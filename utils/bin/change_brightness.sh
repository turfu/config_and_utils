#!/bin/bash
#
# usage: change_brightness.sh FACTOR

set -e -u

source /usr/local/share/bash-utils/util.sh

test $# == 1
declare -r factor="$1"
is_integer "$factor"

cd /sys/class/backlight/intel_backlight
brightness_lvl="$(<brightness)"
is_integer "$brightness_lvl"

brightness="$((brightness_lvl * factor / 100))"
if [ "$brightness" -lt 10 ]; then
	brightness=10
fi

echo "$brightness" > brightness
